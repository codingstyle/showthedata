var request = require('request-promise');

var Get = async (uri, qs) => await request({uri: uri, qs: qs});
Get.json = async (uri, qs) => await request({uri: uri, qs: qs, json: true});

module.exports = Get;
