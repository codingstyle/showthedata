var data = {
  num: 1234,
};

//data//

var vue = new Vue({
  el: '#vue',
  data: data,
  computed: {
    mathFormula: function() {
      return (num + 45) * 23;
    },
  },
  methods: {
    callAPI: function(param = "none") {
      api('/example', {param1: 23, param2: param}, function(result) {
      	console.log("result is", result);
	      $("#testBtn").text(result.param1);
      }, function(error){
      	console.log("error ", error);
      });
    },
  },
});
