
$('.check').on('click', function() {
    if ( $(this).hasClass("checked") ) {
        $(this).removeClass("checked");
        $(this).addClass("check");

    } else {
        $(this).removeClass("check");
        $(this).addClass("checked");
    }
});


function set_chart (red, green, blue) {
    $("#red-dis").stop();
    $("#blu-dis").stop();
    $("#gre-dis").stop();

    $("#red-dis").animate({width: red.width}, 80 );
    $("#gre-dis").animate({width: green.width}, 80 );
    $("#blu-dis").animate({width: blue.width}, 80 );


    if (blue.width > 153) {
        $("#blu-dis").css("borderWidth", 4);
        $("#blu-dis").css("borderStyle", 'dashed');
    } else {
        $("#blu-dis").css("borderWidth", 1);
        $("#blu-dis").css("borderStyle", 'solid');
    }

    if (red.width > 153) {
        $("#red-dis").css("borderWidth", 4);
        $("#red-dis").css("borderStyle", 'dashed');
    } else {
        $("#red-dis").css("borderWidth", 1);
        $("#red-dis").css("borderStyle", 'solid');
    }

    if (green.width > 153) {
        $("#gre-dis").css("borderWidth", 4);
        $("#gre-dis").css("borderStyle", 'dashed');
    } else {
        $("#gre-dis").css("borderWidth", 1);
        $("#gre-dis").css("borderStyle", 'solid');
    }

    $("#chemical_num").html(red.num + " ug/m<sup>3</sup>");
    $("#chemical_num").css("color", red.text.color ? red.text.color : "#ffffff");
    $("#biological_num").html(green.num + " ppb");
    $("#biological_num").css("color", green.text.color ? green.text.color : "#ffffff");
    $("#physical_num").html(blue.num + " ppb");
    $("#physical_num").css("color", blue.text.color ? blue.text.color : "#ffffff");
}

$("#slider_time").slider({
    orientation: "horizontal",
    range: "min",
    max: 23,
    min: 0,
    value: 0,
    slide: refreshSwatch,
    change: refreshSwatch
})

$("#times").change(function() {
    var range_select = $("#times").val();
    var max = 0;
    switch (range_select) {
        case "day":
            max = 23;
            break;
        case "week":
            max = 13;
            break;
        case "month":
            max = 30;
            break;
    }
    $("#slider_time").slider({
        max: max,
        value: 0
    })
})

function refreshSwatch(event, ui) {
    var slider_time = ui.value;
    $("#slider_num").text(slider_time);
}

function times_chart(label, pm, o3, so2) {
    var ctx = document.getElementById("myChart").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: label,
            datasets: [{
                label: 'PM2.5',
                borderColor: 'rgb(0, 255, 0)',
                data: pm
            }, {
                label: 'Ozone',
                borderColor: 'rgb(0, 0, 255)',
                data: o3
            }, {
                label: 'Sulphur Dioxide',
                borderColor: 'rgb(255, 0, 0)',
                data: so2
            }]
        },

        // Configuration options go here
        options: {}
    });
}
var label = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
var pm = [0, 10, 5, 12, 9, 35, 30];
var o3 = [20, 40, 30, 30, 8, 22, 40];
var so2 = [4, 20, 5, 22, 2, 37, 20];
times_chart(label, pm, o3, so2);


function start() {
    $(".start_page").addClass("remove");
}
