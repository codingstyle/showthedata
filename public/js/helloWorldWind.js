let dangerLevel = [
  {color: "#00CC00", name: 'Good'},
  {color: "#FFFF00", name: 'Moderate'},
  {color: "#FF6600", name: 'Unhealthy for sensetive group'},
  {color: "#FF0000", name: 'unhealthy'},
  {color: "#99004C", name: 'very Unhealthy'},
  {color: "#7E0023", name: 'Hazardous'}];
let PM2dot5 = [
  {lowerBound: 0, upperBound: 35, weight: 0.40},
  {lowerBound: 35, upperBound: 75, weight: 0.60},
  {lowerBound: 75, upperBound: 185, weight: 0.0},
  {lowerBound: 185, upperBound: 304, weight: 0.0},
  {lowerBound: 304, upperBound: 604},
  {lowerBound: 604,upperBound: 10000}];
let SO2 = [
  {lowerBound: 0, upperBound: 15.4, weight: 0.40},
  {lowerBound: 15.4, upperBound: 35.4, weight: 0.60},
  {lowerBound: 35.4, upperBound: 54.4, weight: 0.0},
  {lowerBound: 54.4, upperBound: 150.4, weight: 0.0},
  {lowerBound: 150.4, upperBound: 250.4},
  {lowerBound: 250.4, upperBound: 10000}];
let O3 = [
  {lowerBound: 0, upperBound: 54, weight: 0.40},
  {lowerBound: 54, upperBound: 70, weight: 0.60},
  {lowerBound: 70, upperBound: 85, weight: 0.0},
  {lowerBound: 85, upperBound: 105, weight: 0.0},
  {lowerBound: 105, upperBound: 200},
  {lowerBound: 200, upperBound: 10000}];

var wwd = new WorldWind.WorldWindow("canvasOne");
wwd.addLayer(new WorldWind.BMNGOneImageLayer());
wwd.addLayer(new WorldWind.BMNGLandsatLayer());
//wwd.addLayer(new WorldWind.CompassLayer());
wwd.addLayer(new WorldWind.CoordinatesDisplayLayer(wwd));
wwd.addLayer(new WorldWind.ViewControlsLayer(wwd));
wwd.goTo(new WorldWind.Position(23.629364, 120.895240, 634000));

var markLayer = new WorldWind.RenderableLayer("markLayer");
wwd.addLayer(markLayer);

api.get('/taiwanAIR', {}, function (data) {
  var max = {};
  var min = {};
  console.log(data);

  function mapTo255(valueProfile, val) {
    if (isNaN(val))
      return 0;

    var result = 0;
    for (var i = 0; i <= 3; i++) {
      if (val >= valueProfile[i].upperBound) {
        result += 255 * valueProfile[i].weight;
      }
      else {
        var range = valueProfile[i].upperBound - valueProfile[i].lowerBound;
        result += 255 * valueProfile[i].weight * (val - valueProfile[i].lowerBound) / range;
        break;
      }
    }

    return Math.floor(result);
  }

  var forEachCount = 0, dataLength = data.length;
  data.forEach( data => {
    var markPosition = new WorldWind.Position(data.Latitude, data.Longitude, 0);
    var mark = new WorldWind.Placemark(markPosition);
    mark.altitudeMode = WorldWind.CLAMP_TO_GROUND;

    var red = mapTo255(PM2dot5, data['PM2.5']);
    var green = mapTo255(SO2, data.SO2);
    var blue = mapTo255(O3, data.O3);
    {
      let canvas = document.createElement("canvas"),
        ctx2d = canvas.getContext("2d"),
        size = 64, c = size / 2 - 0.5, innerRadius = 5, outerRadius = 20;
      canvas.width = size;
      canvas.height = size;
      let gradient = ctx2d.createRadialGradient(c, c, innerRadius, c, c, outerRadius);
      gradient.addColorStop(0, `rgba(${red}, ${green}, ${blue}, 1)`);
      gradient.addColorStop(0.6, `rgba(${red}, ${green}, ${blue}, 0.3)`);
      gradient.addColorStop(0.95, `rgba(${red}, ${green}, ${blue}, 1)`);
      gradient.addColorStop(1, `rgba(${red}, ${green}, ${blue}, 1)`);
      ctx2d.fillStyle = gradient;
      ctx2d.arc(c, c, outerRadius, 0, 2 * Math.PI, false);
      ctx2d.fill();

      var markAttributes = new WorldWind.PlacemarkAttributes();
      markAttributes.imageSource = new WorldWind.ImageSource(canvas);
      markAttributes.color = new WorldWind.Color(red, green, blue);
      markAttributes.data = data;
      markAttributes.imageScale = 1;
      mark.attributes = markAttributes;
    }

    {
      let canvas = document.createElement("canvas"),
        ctx2d = canvas.getContext("2d"),
        size = 64, c = size / 2 - 0.5, innerRadius = 5, outerRadius = 20;
      canvas.width = size;
      canvas.height = size;
      let gradient = ctx2d.createRadialGradient(c, c, innerRadius, c, c, outerRadius);
      gradient.addColorStop(0, `rgba(${red}, ${green}, ${blue}, 1)`);
      gradient.addColorStop(0.55, `rgba(${red}, ${green}, ${blue}, 0.3)`);
      gradient.addColorStop(0.95, `rgba(255, 255, 255, 1)`);
      gradient.addColorStop(1, `rgba(255, 255, 255, 1)`);
      ctx2d.fillStyle = gradient;
      ctx2d.arc(c, c, outerRadius, 0, 2 * Math.PI, false);
      ctx2d.fill();

      var highlightMarkAttributes = new WorldWind.PlacemarkAttributes(markAttributes);
      highlightMarkAttributes.imageSource = new WorldWind.ImageSource(canvas);
      highlightMarkAttributes.imageScale = 1.3;
      mark.highlightAttributes = highlightMarkAttributes;
    }
    mark.eyeDistanceScaling = true;
    markLayer.addRenderable(mark);
    if (++forEachCount == dataLength - 1) {
      wwd.redraw();
    }
  });
});

var highlightedItem = null;

function handlePick(o) {

  var x = o.clientX, y = o.clientY;
  var redrawRequired = false;
  if (highlightedItem) {
    redrawRequired = true;
    highlightedItem.highlighted = false;
    highlightedItem = null;
  }


  var pickList = wwd.pick(wwd.canvasCoordinates(x, y));
  if (!pickList.topPickedObject().isTerrain) {
    item = pickList.topPickedObject().userObject;
    item.highlighted = true;
    highlightedItem = item;
    item.attributes.condition = {};
    for (var i = 0; i <= 5; i++) {
      if (item.attributes.data['PM2.5'] <= PM2dot5[i].upperBound && item.attributes.data['PM2.5'] >= PM2dot5[i].lowerBound) {
        item.attributes.condition['PM2.5'] = i;
        break;
      }
    }
    for (var i = 0; i <= 5; i++) {
      if (item.attributes.data['SO2'] <= SO2[i].upperBound && item.attributes.data['SO2'] >= SO2[i].lowerBound) {
        item.attributes.condition['SO2'] = i;
        break;
      }
    }
    for (var i = 0; i <= 5; i++) {
      if (item.attributes.data['O3'] <= O3[i].upperBound && item.attributes.data['O3'] >= O3[i].lowerBound) {
        item.attributes.condition['O3'] = i;
        break;
      }
    }
    set_chart({num: item.attributes.data['PM2.5'], text: dangerLevel[item.attributes.condition['PM2.5']], width: item.attributes.color.red},
              {num: item.attributes.data['SO2'], text: dangerLevel[item.attributes.condition['SO2']], width: item.attributes.color.green},
              {num: item.attributes.data['O3'], text: dangerLevel[item.attributes.condition['O3']], width: item.attributes.color.blue});
  }
  else {
    set_chart({num: 0, text: {}, width: 0},
              {num: 0, text: {}, width: 0},
              {num: 0, text: {}, width: 0});
  }

  if (redrawRequired) {
    wwd.redraw(); // redraw to make the highlighting changes take effect on the screen
  }
}

wwd.addEventListener("mousemove", handlePick);
var tapRecognizer = new WorldWind.TapRecognizer(wwd, handlePick);

