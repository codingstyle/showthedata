// Create app
var api = require('../api')('/');
var config = require('../config');
var Mongo = require('../utils/Mongo');
var FS = require('../utils/FS');
var Exec = require('../utils/Exec');
var randomstring = require("randomstring");
var Get = require('../utils/Get');
var apiCache = require('../models/Cache');


// Include models


// example

api.get('/example', (req, res) => {
  res.render({}, 'public/example.html');
});

api.get('/earth', (req, res) => {
  res.render({}, 'public/earth.html');
});

api.get('/taiwanAIR', async (req, res) => {
  var data = apiCache.getCache('http://opendata.epa.gov.tw/webapi/Data/REWIQA/?format=json');
  data.push({
    'PM2.5': "11",
    SO2: "252",
    O3: "49",
    Latitude: 23.330205,
    Longitude: 120.995213
  });
  res.ok(data);
});

api.get('/example.js', (req, res) => {
  res.data({param1: "test", param2: 123}, 'public/example.js');
});

api.post('/example', {param1: 'int', param2: /.+/}, (req, res) => {
  res.ok({param1: req.body.param1, param2: req.body.param2});
});



// Mongo example

api.get('/add', (req, res) => {
  res.ok(Mongo.add('test', {name: req.query.name}));
});

api.get('/get', (req, res) => {
  res.ok(Mongo.get('test', req.query._id));
});

api.get('/set', (req, res) => {
  res.ok(Mongo.set('test', req.query._id, {name: req.query.name}));
});

api.get('/del', (req, res) => {
  res.ok(Mongo.del('test', req.query._id));
});


module.exports = api;
