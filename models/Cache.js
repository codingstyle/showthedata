var Get = require('../utils/Get.js');

var apiCache = {};

var cacheList = [
  {
    url: `http://opendata.epa.gov.tw/webapi/Data/REWIQA/?format=json`,
    data: {},
  },
];
(async () => {
  console.log('debug');
  cachePromiseList = [];
  cacheList.forEach( cache => {
    cachePromiseList.push(Promise.resolve(Get.json(cache.url)));
  });

  await Promise.all(cachePromiseList)
    .then( data => {
      for (i in data) {
        cacheList[i].data = data[i];
      }
    })
    .catch(e => {
      console.log(e);
    });

  cache();
})();

(function cache() {
  setTimeout(async () => {
    cachePromiseList = [];
    cacheList.forEach( cache => {
      cachePromiseList.push(Promise.resolve(Get.json(cache.url)));
    });

    await Promise.all(cachePromiseList)
      .then( data => {
        for (i in data) {
          cacheList[i].data = data[i];
        }
      })
      .catch(e => {
        console.log(e);
      });

    cache();
  }, 600000);
})();

apiCache.getCache = url => {
  for (i in cacheList) {
    if (cacheList[i].url === url)
      return cacheList[i].data;
  }
  return;
};

module.exports = apiCache;
