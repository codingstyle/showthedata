# Getting start

## Install Node.js on Mac / Windows
Download and install Node.js 8.11.x from https://nodejs.org/en/download/.
```bash
$ npm install -g pm2
```

## Install Node.js on Ubuntu
```bash
$ sudo apt-get install npm wget
$ sudo npm install -g n pm2
$ sudo n 8.11
```
# Usage
```bash
$ vi config.js //edit setting ex. listen port
$ npm install
$ node www
^C
$ npm start
```

# Access ShowTheData Web Page
```bash
http://localhost:8080/earth
```

# 框架一瞥
當下了`npm start`之後，`npm`會去讀取**package.json**裡面的script  

    "scripts": {
        "start": "pm2 start www --watch --merge-logs"
      },
**start**參數將會啟動**www**  

    var config = require('./config');
    var server = require('./io');
    var debug = require('debug')('api:server');

    /**
     * Get port from environment and store in Express.
     */

    var port = normalizePort(process.env.PORT || config.listen);

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
**www**會讀取**config**裡的listen來決定port，利用**server**物件來啟動HTTP server  

    var app = require('./app');
    var config = require('./config');
    var fs = require('fs');
    var server;
    if (config.https)
      server = require('https').createServer({
        key: fs.readFileSync(config.ssl_key),
        cert: fs.readFileSync(config.ssl_cert)
      }, app);
    else
      server = require('http').createServer(app);
**io.js**會讀取**config**裡的https來決定要不要開啟加密傳輸，用**app**當作HTTP server的參數  

    // Include user module
    .use('/', require('./routes/index'))
    .use('/car', require('./routes/car'))
**app.js**設定URL的路由，http://(host)/ 開頭的去找**routes/index**  

    api.get('/example', (req, res) => {
      res.render({}, 'public/example.html');
    });
    
    api.get('/example.js', (req, res) => {
      res.data({param1: "test", param2: 123}, 'public/example.js');
    });
    
    api.post('/example', {param1: 'int', param2: /.+/}, (req, res) => {
      res.ok({param1: req.body.param1, param2: req.body.param2});
    });
**routes/index.js**包含細部路由設定

